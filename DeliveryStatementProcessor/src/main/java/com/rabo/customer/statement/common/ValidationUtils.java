package com.rabo.customer.statement.common;

import java.util.Map;

import com.rabo.customer.statement.vo.CustomerRecord;

/**
 * Utils method to validate transaction Reference and Endbalance Euro
 * 
 * @author Loganathan
 *
 */
public class ValidationUtils {

	private ValidationUtils() {
		// TODO Auto-generated constructor stub
	}

	// Validate EndBalance Euro
	public static boolean validateEndEuro(CustomerRecord record) {

		boolean result = false;

		if (!record.getStartBalance().add(record.getMutation()).equals(record.getEndBalance())) {

			result = true;

		}

		return result;
	}

	// Validate unique Transaction Reference no
	public static boolean validateTrxReference(CustomerRecord record, Map<String, String> isUniqueTrxMap) {

		boolean result = false;

		Object isExist = isUniqueTrxMap.put(record.getTransactionRef(), record.getTransactionRef());

		if (isExist != null) {
			result = true;
		}

		return result;

	}

}
