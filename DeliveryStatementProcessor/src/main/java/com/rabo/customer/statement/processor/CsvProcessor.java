package com.rabo.customer.statement.processor;

import static com.rabo.customer.statement.common.FileHandlerConstants.ACCOUNT_NO;
import static com.rabo.customer.statement.common.FileHandlerConstants.DESCRIPTION;
import static com.rabo.customer.statement.common.FileHandlerConstants.ENDBALANCE_VALIDATION_FAILED;
import static com.rabo.customer.statement.common.FileHandlerConstants.END_BALANCE;
import static com.rabo.customer.statement.common.FileHandlerConstants.MUTATION;
import static com.rabo.customer.statement.common.FileHandlerConstants.REFERENCE;
import static com.rabo.customer.statement.common.FileHandlerConstants.START_BALANCE;
import static com.rabo.customer.statement.common.FileHandlerConstants.UNIQUE_TRANSACTIONREF_FAILED;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabo.customer.statement.common.ValidationUtils;
import com.rabo.customer.statement.exception.ProcessorException;
import com.rabo.customer.statement.vo.CustomerRecord;

/**
 * Processor to read data from CSV, then validate two criteria which are
 * uniqueTransactionReference and Sum of StartBalance Euro and Mutatation should
 * be equal to EndBalanceEuro
 * 
 * @author Loganathan
 *
 */

public class CsvProcessor extends BaseProcessor {

	static Logger logger = LoggerFactory.getLogger(CsvProcessor.class);
	
	private List<CustomerRecord> customerRecordList = new ArrayList<>();

	public CsvProcessor() {
		super();
	}

	/**
	 * method to read and validate transaction reference and endbalance Euro. at the
	 * end, generated collection with invalid statements
	 */
	@Override
	public List<CustomerRecord> readAndValidateFile(String fileName) throws ProcessorException {

		HashMap<String, String> hmUniqueTrxReference = new HashMap<>();

		try (CSVParser parser = new CSVParser(new InputStreamReader(new FileInputStream(fileName)),
				CSVFormat.DEFAULT.withHeader())) {
			for (CSVRecord record : parser) {
 
				CustomerRecord customerRecord = new CustomerRecord();
				customerRecord.setTransactionRef(record.get(REFERENCE));
				customerRecord.setAccountNumber(record.get(ACCOUNT_NO));
				customerRecord.setStartBalance(new BigDecimal(record.get(START_BALANCE)));
				customerRecord.setDesriptoin(record.get(DESCRIPTION));
				customerRecord.setMutation(new BigDecimal(record.get(MUTATION)));
				customerRecord.setEndBalance(new BigDecimal(record.get(END_BALANCE)));

				if (ValidationUtils.validateTrxReference(customerRecord, hmUniqueTrxReference)) {
					customerRecord.setValidationError(UNIQUE_TRANSACTIONREF_FAILED);
					customerRecordList.add(customerRecord);
				}
				if (ValidationUtils.validateEndEuro(customerRecord)) {
					customerRecord.setValidationError(ENDBALANCE_VALIDATION_FAILED);
					customerRecordList.add(customerRecord);
				}
				
			}
		} catch (IOException e) {
			logger.error("CSVProcessor Exception in ReadAndEvaluate",e);
			
			throw new ProcessorException();
		} 

		return customerRecordList;

	}

}
