package com.rabo.customer.statement.vo;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;


//this domain used for XML
@XmlRootElement
public class Record {
	
	private String transactionRef;
	private String accountNumber;

	private BigDecimal startBalance;

	private BigDecimal mutation;
	
	private BigDecimal endBalance;
	private String description;
	
	@XmlAttribute(name="reference")
	public String getTransactionRef() {
		return transactionRef;
	}
	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	
	
	public BigDecimal getStartBalance() {
		return startBalance;
	}
	public void setStartBalance(BigDecimal startBalance) {
		this.startBalance = startBalance;
	}
	 
	public BigDecimal getMutation() {
		return mutation;
	}
	public void setMutation(BigDecimal mutation) {
		this.mutation = mutation;
	}
	 
	public BigDecimal getEndBalance() {
		return endBalance;
	}
	public void setEndBalance(BigDecimal endBalance) {
		this.endBalance = endBalance;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	@Override
	public String toString() {
		return "Record [transactionRef=" + transactionRef + ", accountNumber=" + accountNumber + ", startBalance="
				+ startBalance + ", mutation=" + mutation + ", endBalance=" + endBalance + ", description="
				+ description + "]";
	}

}
