package com.rabo.customer.statement;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabo.customer.statement.common.CsvFileWriter;
import com.rabo.customer.statement.exception.ProcessorException;
import com.rabo.customer.statement.exception.TechnicalException;
import com.rabo.customer.statement.factory.FileProcessorFactory;
import com.rabo.customer.statement.processor.IFileProcessor;
import com.rabo.customer.statement.vo.CustomerRecord;

/**
 * main method to check invalid input file and start processing as per file type
 * @author Loganathan
 *
 */
public class ProcessStatement {

	static Logger logger = LoggerFactory.getLogger(ProcessStatement.class);

	public static void main(String[] args) {

		// Invalid Input file
		if (args.length == 0) {
			logger.error("No statement file for processing");
			System.exit(1);
		}
		String directoryName = args[0];

		ProcessStatement processStatement = new ProcessStatement();

		processStatement.processFile(directoryName);
	}

	/**
	 * get processor type and call readAndValidation and generate report
	 * 
	 * @param inputFile
	 */
	public void processFile(String directoryName) {
		List<CustomerRecord> invalidRecords = new ArrayList<>();

		try {

			List<Path> files = fetchFileName(directoryName);

			logger.info("Monthly customer statement validation started");

			for (Path file : files) {

				// get Processor based on file extension
				IFileProcessor processor = FileProcessorFactory.getInstance(file.toString());

				if (processor != null) {
					logger.debug("File processing started for => {}", file);
					invalidRecords.addAll(processFile(processor, file));

				}

			}

			CsvFileWriter.writeCsvFile(invalidRecords);

		} catch (TechnicalException e) {
			logger.error("{} => File processing is aborted", directoryName);
		}

	}

	/**
	 * extract file name from the path provided
	 * 
	 * @param directoryName
	 * @return
	 */
	public List<Path> fetchFileName(String directoryName) {

		List<Path> files = new ArrayList<>();
		try {
			files = Files.list(Paths.get(directoryName)).filter(Files::isRegularFile).collect(Collectors.toList());
		} catch (IOException e) {
			logger.error("Exception in fetching file from path", e);
		}
		return files;

	}

	/**
	 *  call readAndValidatFile method for each file
	 * @param processor
	 * @param file
	 * @return
	 */
	public List<CustomerRecord> processFile(IFileProcessor processor, Path file) {

		List<CustomerRecord> invalidRecords = new ArrayList<>();
		try {
			invalidRecords = processor.readAndValidateFile(file.toString());
		} catch (ProcessorException e) {
			logger.error("Processor error in {}, check log file for details", file);
		}
		return invalidRecords;
	}
}
