package com.rabo.customer.statement.common;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabo.customer.statement.exception.TechnicalException;
import com.rabo.customer.statement.vo.CustomerRecord;

/**
 * 
 * Class used to generate validation failed statement into CSV
 *
 */
public class CsvFileWriter {

	static Logger logger = LoggerFactory.getLogger(CsvFileWriter.class);
	
	// Delimiter used in CSV file
	private static final String NEW_LINE_SEPARATOR = "\n";

	// CSV file header
	private static final Object[] FILE_HEADER = { "ValidationError", "TransactionReference", "Description" };

	/**
	 * generate csv report with statement records failed in validation
	 * @param invalidStatements
	 * @throws TechnicalException
	 * @throws IOException
	 */
	public static void writeCsvFile(List<CustomerRecord> invalidStatements) throws TechnicalException {

		// Create the CSVFormat object with "\n" as a record delimiter
		CSVFormat csvFileFormat = CSVFormat.DEFAULT.withRecordSeparator(NEW_LINE_SEPARATOR);

		try (OutputStreamWriter fileWriter = new OutputStreamWriter(new FileOutputStream("InvalidStatements.csv"));
				CSVPrinter csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat)) {

			// Create CSV file header
			csvFilePrinter.printRecord(FILE_HEADER);

			// Write a new customerRecord object list to the CSV file
			for (CustomerRecord invalidStatement : invalidStatements) {
				List<String> customerRecord = new ArrayList<String>();
				customerRecord.add(String.valueOf(invalidStatement.getValidationError()));
				customerRecord.add(invalidStatement.getTransactionRef());
				customerRecord.add(invalidStatement.getDesriptoin());
				csvFilePrinter.printRecord(customerRecord);
			}

			logger.info(
					"Monthly customer statement validation completed. Please check csv report for validation error if any");

		} catch (Exception e) {
			throw new TechnicalException();
		}
	}
}
