package com.rabo.customer.statement.common;

public interface FileHandlerConstants {
	
	// file type constants
	final String CSV = "csv";
	final String XML = "xml";

	//handle validation error in report
	final String UNIQUE_TRANSACTIONREF_FAILED="UniqueTransactionReferenceFailed";
	final String ENDBALANCE_VALIDATION_FAILED="EndBalanceValidationFailed";
	
	//handle CSV file header
	final String REFERENCE="Reference";
	final String ACCOUNT_NO="Account Number";
	final String START_BALANCE="Start Balance";
    final String DESCRIPTION="Description";
	final String MUTATION="Mutation";
	final String END_BALANCE="End Balance";
}
