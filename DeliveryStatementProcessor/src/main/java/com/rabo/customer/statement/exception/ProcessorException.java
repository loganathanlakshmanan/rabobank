package com.rabo.customer.statement.exception;

/**
 * ProcessorException generated for any exception in respective file processing
 * @author Loganathan
 *
 */
public class ProcessorException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1752820995777373243L;
	
	public ProcessorException() {
		super();
	}

}
