package com.rabo.customer.statement.exception;

public class TechnicalException extends Exception {

	/**
	 * Custom exception to handle IO and Other exception to abort file processing
	 */
	private static final long serialVersionUID = 1L;

	public TechnicalException() {
		super();
	}
	
}
