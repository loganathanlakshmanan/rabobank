package com.rabo.customer.statement.processor;

import java.util.List;

import com.rabo.customer.statement.exception.ProcessorException;
import com.rabo.customer.statement.vo.CustomerRecord;

public interface IFileProcessor {

	List<CustomerRecord> readAndValidateFile(String file) throws ProcessorException;
	
}
