package com.rabo.customer.statement.processor;

import java.util.Collections;
import java.util.List;

import com.rabo.customer.statement.exception.ProcessorException;
import com.rabo.customer.statement.vo.CustomerRecord;

public abstract class BaseProcessor implements IFileProcessor {

	public BaseProcessor() {
	}

	@Override
	public List<CustomerRecord> readAndValidateFile(String file) throws ProcessorException {
		return Collections.emptyList();
	}

}
