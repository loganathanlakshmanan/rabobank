package com.rabo.customer.statement.vo;

import java.math.BigDecimal;


//this domain used for CSV file 
public class CustomerRecord {
	private String transactionRef;
	
	@Override
	public String toString() {
		return "Statement [transactionRef=" + transactionRef + ", accountNumber=" + accountNumber + ", startBalance="
				+ startBalance + ", mutation=" + mutation + ", endBalance=" + endBalance + ", desriptoin=" + description
				+ "]";
	}
	private String accountNumber;
	private BigDecimal startBalance;
	private BigDecimal mutation;
	private BigDecimal endBalance;
	private String description;
	
	private String validationError;
	
	public String getTransactionRef() {
		return transactionRef;
	}
	public void setTransactionRef(String transactionRef) {
		this.transactionRef = transactionRef;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public BigDecimal getStartBalance() {
		return startBalance;
	}
	public void setStartBalance(BigDecimal startBalance) {
		this.startBalance = startBalance;
	}
	public BigDecimal getMutation() {
		return mutation;
	}
	public void setMutation(BigDecimal mutation) {
		this.mutation = mutation;
	}
	public BigDecimal getEndBalance() {
		return endBalance;
	}
	public void setEndBalance(BigDecimal endBalance) {
		this.endBalance = endBalance;
	}
	public String getDesriptoin() {
		return description;
	}
	public void setDesriptoin(String desriptoin) {
		this.description = desriptoin;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((transactionRef == null) ? 0 : transactionRef.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CustomerRecord other = (CustomerRecord) obj;
		if (transactionRef == null) {
			if (other.transactionRef != null)
				return false;
		} else if (!transactionRef.equals(other.transactionRef)) {
			return false;
		}
			
		return true;
	}
	public String getValidationError() {
		return validationError;
	}
	public void setValidationError(String validationError) {
		this.validationError = validationError;
	}
	

}
