package com.rabo.customer.statement.processor;

import static com.rabo.customer.statement.common.FileHandlerConstants.ENDBALANCE_VALIDATION_FAILED;
import static com.rabo.customer.statement.common.FileHandlerConstants.UNIQUE_TRANSACTIONREF_FAILED;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.rabo.customer.statement.common.ValidationUtils;
import com.rabo.customer.statement.exception.ProcessorException;
import com.rabo.customer.statement.vo.CustomerRecord;
import com.rabo.customer.statement.vo.Record;
import com.rabo.customer.statement.vo.Records;

/**
 * Processor to read data from XML, then validate two criteria which are
 * uniqueTransactionReference and Sum of StartBalance Euro and Mutatation should
 * be equal to EndBalanceEuro
 * 
 * @author Loganathan
 *
 */
public class XMLProcessor extends BaseProcessor {

	static Logger logger = LoggerFactory.getLogger(XMLProcessor.class);

	private List<CustomerRecord> customerRecordList = new ArrayList<>();

	public XMLProcessor() {
		super();
	}

	/**
	 * method to read and validate transaction reference and endbalance Euro. at the
	 * end, generated collection with invalid statements
	 */
	@Override
	public List<CustomerRecord> readAndValidateFile(String fileName) throws ProcessorException {
		Records records;
		File inputFile = new File(fileName);
		JAXBContext jaxbContext;
		HashMap<String, String> hmUniqueTrxReference = new HashMap<>();

		try {
			jaxbContext = JAXBContext.newInstance(Records.class);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			records = (Records) unmarshaller.unmarshal(inputFile);
			for (Record record : records.getRecords()) {

				CustomerRecord custRecord = new CustomerRecord();
				custRecord.setTransactionRef(record.getTransactionRef());
				custRecord.setAccountNumber(record.getAccountNumber());
				custRecord.setDesriptoin(record.getDescription());
				custRecord.setStartBalance(record.getStartBalance());
				custRecord.setMutation(record.getMutation());
				custRecord.setEndBalance(record.getEndBalance());

				if (ValidationUtils.validateTrxReference(custRecord, hmUniqueTrxReference)) {
					custRecord.setValidationError(UNIQUE_TRANSACTIONREF_FAILED);
					customerRecordList.add(custRecord);
				}
				if (ValidationUtils.validateEndEuro(custRecord)) {
					custRecord.setValidationError(ENDBALANCE_VALIDATION_FAILED);
					customerRecordList.add(custRecord);
				}

			}

		} catch (JAXBException e) {
			logger.error("XMLProcessor Exception in ReadAndEvaluate", e);
			throw new ProcessorException();
		} 

		return customerRecordList;

	}

}
