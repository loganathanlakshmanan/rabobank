package com.rabo.customer.statement.vo;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

//this domain used for XML data in list
@XmlRootElement(name = "records")
@XmlAccessorType (XmlAccessType.FIELD)
public class Records {

	@XmlElement(name = "record")
	private List<Record> recordList=null;

	public List<Record> getRecords() {
		return recordList;
	}

	public void setRecords(List<Record> records) {
		this.recordList = records;
	}
	
}
