package com.rabo.customer.statement.factory;

import com.rabo.customer.statement.exception.TechnicalException;
import com.rabo.customer.statement.processor.CsvProcessor;
import com.rabo.customer.statement.processor.IFileProcessor;
import com.rabo.customer.statement.processor.XMLProcessor;
import static com.rabo.customer.statement.common.FileHandlerConstants.*;

import org.apache.commons.io.FilenameUtils;

public class FileProcessorFactory {

	private FileProcessorFactory() {
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * create processor class as per input file extension
	 * @param inputFile
	 * @return
	 * @throws TechnicalException
	 */
	public static IFileProcessor getInstance(String inputFile) throws TechnicalException{
		
		String fileExtension = FilenameUtils.getExtension(inputFile);
		
		//TO-DO add singleton logic to generate processor object
		if (CSV.equalsIgnoreCase(fileExtension)) {

			return new CsvProcessor();

		} else if (XML.equalsIgnoreCase(fileExtension)) {

			return new XMLProcessor();
		}
		return null;
		

		
	}
}
