Problem Statement
Rabobank receives monthly deliveries of customer statement records. This information is delivered in two formats, CSV and XML. These records need to be validated.
There are two validations:
•	all transaction references should be unique
•	the end balance needs to be validated
At the end of the processing, a report needs to be created which will display both the transaction reference and description of each of the failed records.

Design Notes

•	Use library to read data from CSV and XML file 

	o	For XML – selected jaxb between JaxB, Xstream, Apache XML bean libraries as it is available in jdk
	o	For CSV  -- considered Apache common CSV as there is only loading and writing operation required with simple operation

•	Used directory looping for processing each file in the directory

•	Used sequence file processing

•	Used abstract base class and implement method defined in Interface

•	Used constants in single file

•	If exception in file processing, log exception details and process next file

•	Used factory of processor for each file type which will extendable for new file type

	o	Generate Processor class with method
	
•	Output of validation should be generated in CSV

	o	Consolidate details of invalid records into one file with description of failed validation logic
	o	Generate output in CSV

Score for improvement

•	Use Spring Batch to handle different file type processing in simple configuration changes.

•	Unit test case to be added and configure in maven as part of build goal

•	For large file size, fetch record based on limit for processing

 
